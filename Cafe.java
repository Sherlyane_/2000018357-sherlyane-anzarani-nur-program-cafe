package com.company;
import java.util.Scanner;

public class Cafe {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        int total;
        int bayar = 0;
        String menu;
        int h[] = {38000, 45000, 25000, 55000, 20000, 18000, 35000, 25000, 12000, 18000, 15000};
        String carabayar;
        String hadiah = null;
        int uang,kembalian;
        Customer cs = new Customer();

        System.out.println(" ");
        System.out.println("============ MENU SHERLY'S CAFE ===========");
        System.out.println("||                                       ||");
        System.out.println("||            ~~~~~~~~~~~                ||");
        System.out.println("||              MAKANAN                  ||");
        System.out.println("||            ~~~~~~~~~~~                ||");
        System.out.println("||   1. Burger                  38.000   ||");
        System.out.println("||   2. Pizza                   45.000   ||");
        System.out.println("||   3. Carbonara               25.000   ||");
        System.out.println("||   4. Steak                   55.000   ||");
        System.out.println("||                                       ||");
        System.out.println("||            ~~~~~~~~~~~                ||");
        System.out.println("||              MINUMAN                  ||");
        System.out.println("||            ~~~~~~~~~~~                ||");
        System.out.println("||   5. Milk Shake               20.000  ||");
        System.out.println("||   6. Ice Tea                  18.000  ||");
        System.out.println("||   7. Smoothie                 35.000  ||");
        System.out.println("||   8. Cappuchino               25.000  ||");
        System.out.println("||                                       ||");
        System.out.println("||            ~~~~~~~~~~~                ||");
        System.out.println("||              DESSERT                  ||");
        System.out.println("||            ~~~~~~~~~~~                ||");
        System.out.println("||   9.  Ice Cream Cone          12.000  ||");
        System.out.println("||   10. Puding                  18.000  ||");
        System.out.println("||   11. Pancake                 15.000  ||");
        System.out.println("||                                       ||");
        System.out.println("===========================================");
        System.out.println(" ");

        for (String menpes = "Y"; menpes.equals("Y") || menpes.equals("y"); ) {
            System.out.println("-------------------------------------------------------------------------------");
            System.out.print("Masukan Pesanan Anda   :  ");
            int pesanan = input.nextInt();

            System.out.print("Masukan Jumlah Pesanan :  ");
            int jumlah = input.nextInt();
            System.out.println("-------------------------------------------------------------------------------");

            if (pesanan == 1) {
                menu = " Burger ";
                total = h[1] * jumlah;
                bayar = bayar + h[1] * jumlah;
                System.out.println("Menu Pesanan Anda : " + menu + "    x" + jumlah + "        Rp. " + total);
            } else if (pesanan == 2) {
                menu = " Pizza ";
                total = h[2] * jumlah;
                bayar = bayar + h[2] * jumlah;
                System.out.println("Menu Pesanan Anda : " + menu + "    x" + jumlah + "     Rp. " + total);
            } else if (pesanan == 3) {
                menu = " Carbonara  ";
                total = h[3] * jumlah;
                bayar = bayar + h[3] * jumlah;
                System.out.println("Menu Pesanan Anda : " + menu + "    x" + jumlah + "     Rp. " + total);
            } else if (pesanan == 4) {
                menu = " Steak ";
                total = h[4] * jumlah;
                bayar = bayar + h[4] * jumlah;
                System.out.println("Menu Pesanan Anda : " + menu + "    x" + jumlah + "     Rp. " + total);
            } else if (pesanan == 5) {
                menu = " Milk Shake  ";
                total = h[5] * jumlah;
                bayar = bayar + h[5] * jumlah;
                System.out.println("Menu Pesanan Anda : " + menu + "    x" + jumlah + "     Rp. " + total);
            } else if (pesanan == 6) {
                menu = " Ice Tea   ";
                total = h[6] * jumlah;
                bayar = bayar + h[6] * jumlah;
                System.out.println("Menu Pesanan Anda : " + menu + "    x" + jumlah + "     Rp. " + total);
            } else if (pesanan == 7) {
                menu = " Smoothie  ";
                total = h[7] * jumlah;
                bayar = bayar + h[7] * jumlah;
                System.out.println("Menu Pesanan Anda : " + menu + "    x" + jumlah + "     Rp. " + total);
            } else if (pesanan == 8) {
                menu = " Cappuchino  ";
                total = h[8] * jumlah;
                bayar = bayar + h[8] * jumlah;
                System.out.println("Menu Pesanan Anda : " + menu + "    x" + jumlah + "     Rp. " + total);
            } else if (pesanan == 9) {
                menu = " Ice Cream Cone   ";
                total = h[9] * jumlah;
                bayar = bayar + h[9] * jumlah;
                System.out.println("Menu Pesanan Anda : " + menu + "    x" + jumlah + "     Rp. " + total);
            } else if (pesanan == 10) {
                menu = " Puding  ";
                total = h[10] * jumlah;
                bayar = bayar + h[10] * jumlah;
                System.out.println("Menu Pesanan Anda : " + menu + "    x" + jumlah + "     Rp. " + total);
            } else if (pesanan == 11) {
                menu = " Pancake ";
                total = h[11] * jumlah;
                bayar = bayar + h[11] * jumlah;
                System.out.println("Menu Pesanan Anda : " + menu + "    x" + jumlah + "     Rp. " + total);
            } else {
                System.out.println("Maaf menu yang anda pilih tidak tersedia");
            }

            System.out.println(" ");
            System.out.print("Apakah anda ingin memesan lagi? Y/T : ");
            menpes = input.next();
            System.out.println(" ");

        }
        System.out.print("Silahkan Pilih Cara Bayar (Cash/Debit) :  ");
        carabayar = input.next();
        System.out.println("Total Pesanan Anda : " + bayar);


        if (bayar >= 200000) {
            System.out.println(" ");
            System.out.println("===================================================");
            System.out.println("|| SELAMAT TOTAL BELANJA ANDA LEBIH DARI 200RIBU ||");
            System.out.println("||        ANDA BERHAK MENDAPATKAN HADIAH         ||");
            System.out.println("===================================================");
            System.out.println(" ");

            System.out.println("--------------------------");
            System.out.println("|  REWARD SHERLY'S CAFE  |");
            System.out.println("| 1. Tumblr              |");
            System.out.println("| 2. Payung              |");
            System.out.println("| 3. Jacket              |");
            System.out.println("--------------------------");

            System.out.print("Silahkan Masukan Pilihan Anda :   ");
            hadiah = input.next();
        }

        System.out.print("Masukan Uang Pembayaran Anda :    ");
        uang = input.nextInt();
        kembalian = uang-bayar;

        System.out.println(" ");
        System.out.println("....................Proses Mencetak Struk....................");
        System.out.println(".............................................................");
        System.out.println(".............................................................");
        System.out.println(" ");

        System.out.println(" ");
        System.out.println("______________________________");
        System.out.println("|        SHERLY'S CAFE       |");
        System.out.println("| Jl.Raya Banjar, Jawa Barat |");
        System.out.println("|       Telp 0210292773      |");
        System.out.println("| -------------------------- |");
        System.out.println("| No Antrian : "+cs.getNoAntrian()+"           |");
        System.out.println("| Nama       : "+cs.getNama()+"      |");
        System.out.println("| No HP      : "+cs.getNohp()+"  |");
        System.out.println("| -------------------------- |");
        System.out.println("|                            |");
        System.out.println("|   Total      : " +bayar+"      |");
        System.out.println("|   Bayar      : " +uang+"      |");
        System.out.println("|   Kembalian  : " +kembalian+"       |");
        System.out.println("|                            |");
        System.out.println("| Pembayaran : " +carabayar+"          |");
        System.out.println("| Reward     : "+hadiah+"        |");
        System.out.println("|                            |");
        System.out.println("|        TERIMAKASIH         | ");
        System.out.println("|     ATAS KEHADIRANNYA      | ");
        System.out.println("______________________________");

    }

}

